<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function index()
	{		
		$this->load->library('session');		
		$this->load->view('login_view');		
	}	
		
	public function checkUserLogin(){
		//echo 'hai';exit;
		//echo '<pre>';print_r($_REQUEST);exit;
		$this->load->library('session');
		
		$varUsername = trim($_REQUEST['username']);
		$varPassword = trim($_REQUEST['password']);
		
		$params = array('username'=>$varUsername,'Password'=>$varPassword);
		$arrLoginInfo = ApiPostHeader($this->config->item('UserLogin'), $params);
		//echo '<pre>';print_r($params);print_r($arrLoginInfo);print_r($_SESSION);echo $this->config->item('UserLogin');exit;				
		if(trim($arrLoginInfo[0]['errcode'])=='0' && trim($arrLoginInfo[0]['errcode'])!=''){			
			if(!isset($_SESSION)) {
				session_start();
			}	
			$_SESSION['userId'] = $arrLoginInfo[0]['user_id'];
			$_SESSION['userName'] = $varUsername;
			//$_SESSION['userEmail'] = $arrLoginInfo[0]['useremail'];			
			$_SESSION['userType'] = $arrLoginInfo['user_type'];
			redirect('dashboard');
			/*if($_SESSION['userId']=="1"){
				redirect('dashboard');		
			}else if($_SESSION['userId']=="2"){
				redirect('issuelist');
			}*/
		}else{
			$this->session->set_flashdata('errmsg','Please enter a valid username & password');
			redirect('login');
		}		
	}
	
	public function getClientIp(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	public function UsersignOut(){
		session_start();
		session_destroy();
		session_unset();
		unset($_SESSION['username']);
		redirect('login');
	}		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */