<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {	
	function __Construct(){
		parent::__Construct ();
		if(!isset($_SESSION)) {
				session_start();
		}		
		if($_SESSION['userName']==''){
			redirect('login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{
		//echo 'hai';
		//echo '<pre>';print_r($_SESSION);exit;		
		
		
		$this->load->library('session');
		$this->load->view('header_view');
		$this->load->view('leftmenu_view');
		$this->load->view('dashboard_view');
		$this->load->view('footer_view');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */