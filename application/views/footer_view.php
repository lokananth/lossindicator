 <footer class="main-footer">
    <!-- <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.3
    </div> -->
    <strong>Copyright &copy; <?php echo date("Y");?> <a href="http://mundio.com" target="_blank">Mundio Mobile</a>.</strong> All rights
    reserved.
  </footer>


</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>

  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-animate.js"></script>
<!-- Angular JS -->
<script src="<?php echo base_url();?>assets/dist/js/aj.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>

<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="<?php echo base_url();?>assets/plugins/chartjs/Chart.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- jQuery Knob -->
<script src="<?php echo base_url();?>assets/plugins/knob/jquery.knob.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/morris/morris.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/canvasjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/excanvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/canvasjs/1.7.0/jquery.canvasjs.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/dist/js/demo.js"></script>
<!-- page script -->
<!-- FLOT CHARTS -->
<script src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.min.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.resize.min.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.pie.min.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.categories.min.js"></script>

<!-- Page script -->
  <script type="text/javascript">
 $(document).ready(function(e){	
/* $('.addShift-list').hide();
 $('.addShift').on('click' , function(){ 
 $('.addShift-list').slideDown();
 });
 $('.closeShift').on('click' , function(){ 
 $('.addShift-list').slideUp();
 });*/
 });

$(function () {
	var url = window.location;
	var element = $('ul#sidebar-menu a').filter(function () {
		return this.href == url || url.href.indexOf(this.href) == 1;
	}).parent().addClass('active').parent().parent().addClass('in');
	if (element.is('li')) {
		element.addClass('active');
	}
});
 </script>
 
<script type="text/javascript">
$(document).ready(function(){
//Date picker
    $('#datepicker,#datepicker2').datepicker({
      autoclose: true
    });
	
	
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
	
	//Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
});
</script>
<script>
$(document).ready(function () {
    $("#example1,#example2,#example3,#example4").DataTable({"responsive": true,
    responsive: true,
    columnDefs: [
        { responsivePriority: 1, targets: 0 },
        { responsivePriority: 2, targets: -1 }
    ]});
    /*$('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "responsive": true,
      "info": true,
      "autoWidth": false
    });*/
  });
</script>
 <script type="text/javascript">
$(document).ready(function(){


    //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: true,
      data: [
        {y: '2010', a: 50, b: 40},
        {y: '2011', a: 75, b: 65},
        {y: '2012', a: 100, b: 90}
      ],
      barColors: ['#00a65a', '#f56954'],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['Online Orders', 'Offline Orders'],
      hideHover: 'auto'
    });
	
	  //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart1',
      resize: true,
      data: [
        {y: '2010', a: 50, b: 40},
        {y: '2011', a: 75, b: 65},
        {y: '2012', a: 100, b: 90}
      ],
      barColors: ['#00a65a', '#f56954'],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['Online Orders', 'Offline Orders'],
      hideHover: 'auto'
    });
	
	  //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart2',
      resize: true,
      data: [
        {y: '2010', a: 50, b: 40},
        {y: '2011', a: 75, b: 65},
        {y: '2012', a: 100, b: 90}
      ],
      barColors: ['#00a65a', '#f56954'],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['Online Orders', 'Offline Orders'],
      hideHover: 'auto'
    });
	
	  //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart3',
      resize: true,
      data: [
        {y: '2010', a: 50, b: 40},
        {y: '2011', a: 75, b: 65},
        {y: '2012', a: 100, b: 90}
      ],
      barColors: ['#00a65a', '#f56954'],
      xkey: 'y',
      ykeys: ['a', 'b'],
      labels: ['Online Orders', 'Offline Orders'],
      hideHover: 'auto'
    });
	
	  // BAR CHART
    // var bar = new Morris.Bar({
      // element: 'bar-chart2',
      // resize: true,
      // data: [
        // {y: '2010', a: 50, b: 40},
        // {y: '2011', a: 75, b: 65},
        // {y: '2012', a: 100, b: 90}
      // ],
      // barColors: ['#00a65a', '#f56954'],
      // xkey: 'y',
      // ykeys: ['a', 'b'],
      // labels: ['Online Orders', 'Offline Orders'],
      // hideHover: 'auto'
    // });
	
});
</script> 
<script type="text/javascript"> 
window.onload = function() { 
	$('#chartContainer').CanvasJSChart({ 
		title: { 
			text: "Software",
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			//horizontalAlign: "right" 
			verticalAlign: "bottom" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			showInLegend: true, 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Device Working",  y: 89.3, legendText: "Device Working"}, 
				{ label: "Device Not Working ",  y: 9.7, legendText: "Device Not Working"  }/*, 
				{ label: "Huawei",   y: 4.0,  legendText: "Huawei" }, 
				{ label: "LG",       y: 3.8,  legendText: "LG Electronics"}, 
				{ label: "Lenovo",   y: 3.2,  legendText: "Lenovo" }, 
				{ label: "Others",   y: 39.6, legendText: "Others" } */
			] 
		} 
		] 
	}); 
	$('#chartContainer1').CanvasJSChart({ 
		title: { 
			text: "Hardware",
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			//horizontalAlign: "right" 
			verticalAlign: "bottom" 
		}, 
		data: [ 
		{ 
			type: "pie", 
			showInLegend: true, 
			toolTipContent: "{label} <br/> {y} %", 
			indexLabel: "{y} %", 
			dataPoints: [ 
				{ label: "Device Working",  y: 89.3, legendText: "Device Working"}, 
				{ label: "Device Not Working ",  y: 9.7, legendText: "Device Not Working"  }/*, 
				{ label: "Huawei",   y: 4.0,  legendText: "Huawei" }, 
				{ label: "LG",       y: 3.8,  legendText: "LG Electronics"}, 
				{ label: "Lenovo",   y: 3.2,  legendText: "Lenovo" }, 
				{ label: "Others",   y: 39.6, legendText: "Others" } */
			] 
		} 
		] 
	}); 
} 
</script> 
</body>
</html>

