<?php //include('top_header.php');?>
<?php //include('left_menu.php');?>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="box  box-info">
			<div class="box-header with-border">
			  <h3 class="box-title">Performance</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form>
					<div class="row clearfix">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
							<div class="form-group">
								<label for="email_address">Performance</label>
								<div class="form-line">
									<input type="text" class="form-control" placeholder="Overall Performance">
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
							<div class="form-group">
								<label for="email_address">Start Date</label>
								<div class="form-line">
									<div class="input-group date">
									  <div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									  </div>
									  <input type="text" class="form-control pull-right" id="datepicker">
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
							<div class="form-group">
								<label for="email_address">End Date</label>
								<div class="form-line">
									<div class="input-group date">
									  <div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									  </div>
									  <input type="text" class="form-control pull-right" id="datepicker2">
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ">
							<div class="form-group">
								<label for="email_address" class="hidden-xs">&nbsp;</label>
								<div class="form-linea">
									<button type="button" class="btn btn-primary btn-sm waves-effect">Search</button>
								</div>
							</div>
						</div>
					</div>
				</form>
		
			</div>
		</div>
		<div class="row">
        <div class="col-lg-3 col-xs-6">
			<div class="row">			
			<!-- QR Code -->	
			<div class="col-lg-12 col-sm-12 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
				<div class="row">
					<div class="col-sm-5">150 <i class="fa fa-caret-up"></i></div><div class="col-sm-5">150 <i class="fa fa-caret-up"></i></div>
				</div>
			</h3>

              <p>Managers</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
			
				</div>		
				<!-- QR Code -->	
				<div class="col-lg-12 col-sm-12 col-xs-12">
					<div class="box  box-info">
						<div class="box-header with-border">
						  <h3 class="box-title">Manager Name</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">			
							<!-- BAR CHART -->
							<div class="chart" id="bar-chart" style="height: 300px;"></div>
						</div>
						<!-- /.box-body -->
					</div>							  
				</div>
				<!-- /.col -->
			</div>
		</div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
			<div class="row">			
			<!-- QR Code -->	
			<div class="col-lg-12 col-sm-12 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>53<sup style="font-size: 20px">%</sup></h3>

              <p>Bounce Rate</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
			
				</div>
				<!-- QR Code -->	
				<div class="col-lg-12 col-sm-12 col-xs-12">
					<div class="box  box-info">
						<div class="box-header with-border">
						  <h3 class="box-title">Manager Name</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">			
							<!-- BAR CHART -->
							<div class="chart" id="bar-chart1" style="height: 300px;"></div>
						</div>
						<!-- /.box-body -->
					</div>							  
				</div>
				<!-- /.col -->
			</div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
			<div class="row">			
			<!-- QR Code -->	
			<div class="col-lg-12 col-sm-12 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>44</h3>

              <p>User Registrations</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
		  
				</div>
				<!-- QR Code -->	
				<div class="col-lg-12 col-sm-12 col-xs-12">
					<div class="box  box-info">
						<div class="box-header with-border">
						  <h3 class="box-title">Manager Name</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">			
							<!-- BAR CHART -->
							<div class="chart" id="bar-chart2" style="height: 300px;"></div>
						</div>
						<!-- /.box-body -->
					</div>							  
				</div>
				<!-- /.col -->
			</div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
			<div class="row">			
			<!-- QR Code -->	
			<div class="col-lg-12 col-sm-12 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
				</div>
			
				<!-- QR Code -->	
				<div class="col-lg-12 col-sm-12 col-xs-12">
					<div class="box  box-info">
						<div class="box-header with-border">
						  <h3 class="box-title">Manager Name</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">			
							<!-- BAR CHART -->
							<div class="chart" id="bar-chart3" style="height: 300px;"></div>
						</div>
						<!-- /.box-body -->
					</div>							  
				</div>
				<!-- /.col -->
        </div>
        <!-- ./col -->
      </div>
		
    </section>
    <!-- /.content -->
	
  </div>
  <!-- /.content-wrapper -->

  
 <?php //include('footer.php'); ?>


 
 