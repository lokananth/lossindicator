<?php include('top_header.php'); ?>
<?php include('left_menu.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Performance Track
        <small>all performance status</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Weekly Performance Track</h3>
			  <div class="box-tools pull-right">
				<!-- <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 peoples are late">3</span> -->
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">             
              <table id="example1" class="table table-bordered table-striped nowrap dt-responsive"  style="width:100%">
                <thead>
                <tr>
                  <th>S.No</th>
                  <th>Destination Name</th>
                  <th>Carrier Manager</th>
                  <th>No Of Mins</th>
                  <th>Total Cost</th>
                  <th>Average Cost</th>
				  <th>Profit / Loss</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>1211</td>
                  <td class="text-aqua">Joined</td>
                  <td>Tester</td>
                  <td>Early</td>
                  <td>Early</td>
                  <td>Early</td>
                  <td>$0.02</td>
                </tr>               
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        
       
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  

 <?php include('footer.php'); ?>
 