<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>:: Loss Limiter Indicator - Portal ::</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Tell the browser to be responsive to screen width favicon.ico-->
  <link rel="icon" href="<?php echo base_url();?>assets/dist/img/favicon.ico" type="image/x-icon"/>
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">
	body {
  background:#00BCD4 !important;
}
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#" class="text-center" ><img src="<?php echo base_url();?>assets/dist/img/logo.png" class="img-responsive" style="max-width:60%;margin:0 auto;" alt="Brand" /></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
	
	<?php if($this->session->flashdata('errmsg')!=''){ ?>
	<div class="alert alert-danger" id="errmsg" >
		<?php echo $this->session->flashdata('errmsg'); ?>
	</div>
	<?php } ?>

    <form action="<?php echo base_url();?>login/checkUserLogin" id="loginForm" method="post">
      <div class="form-group has-feedback">
        <input type="text" id="username" name="username" value="" maxlength="25" autocomplete="off" class="form-control" placeholder="Username" />
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" id="password" name="password" maxlength="25" autocomplete="off" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-12 m-t-10">
          <button type="submit" class="btn btn-info btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="<?php echo base_url();?>assets/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/plugins/iCheck/icheck.min.js"></script>

<script src="<?php echo base_url(); ?>assets/dist/js/jquery.validate.js"></script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  
  $(document).ready(function() {
	 //alert('hai');
	$("#username").focus(function(){
		$("#errmsg").hide();
	 });
	 $("#password").focus(function(){
		$("#errmsg").hide();
	 });	

	$("#username").blur(function(){
		$(this).val(
            $.trim($(this).val())
          );	
 	});
	$("#password").blur(function(){
		$(this).val(
             $.trim($(this).val())
          );
 	});	
  $('#loginForm').validate({
	rules:{
		username:{
			required : true    // Name of the username field
		},
		password:{
			required : true    // Name of the password field
		}
	},
	messages:{
		username:"Please enter your name",
		password:"Please enter your password"
	}
  });
  
  $("#username").blur(function(){
	  $("#username").valid();
  });	
  $("#password").blur(function(){
	  $("#password").valid();
  });
		
  });
  
  $.validator.setDefaults({
    errorElement: "span",
    errorClass: "help-block",
    highlight: function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
    },
    errorPlacement: function (error, element) {
        if (element.parent('.input-group').length || element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
        error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});
</script>
</body>
</html>
