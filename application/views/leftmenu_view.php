 <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" id="sidebar-menu">
        <li class="header">ADMIN</li>
        <li>
          <a href="<?php echo base_url(); ?>dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Performance Track</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="H-Track.php"><i class="fa fa-plus text-aqua"></i>Hourly Track</a></li>
            <li><a href="W-Track.php"><i class="fa fa-table text-green"></i>Weekly Track</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>