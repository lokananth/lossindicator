<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		EllisLab Dev Team
 * @copyright		Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @copyright		Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Array Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/array_helper.html
 */
 
 // ------------------------------------------------------------------------
 if ( ! function_exists('ApiAccessTokenHeader'))
{
	 function ApiAccessTokenHeader($apiType, $apiValue) {				
        $apiURL = $apiType;		
		$apiHeader = array(
			'Authorization: Basic TXVuZGlvSW52ZW50b3J5bWFuYWdlbWVudGFwaTEwMzA='					
		);
		
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiValue));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $apiHeader);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		//curl_setopt($ch,CURLOPT_TIMEOUT, 180);
		curl_setopt($ch,CURLOPT_TIMEOUT, 600);
        $output = curl_exec($ch);
        curl_close($ch);
        //print_r($output);exit;
		//print_r(json_decode($output, true));exit;
        return json_decode($output, true);
    }
}


// ------------------------------------------------------------------------
if ( ! function_exists('ApiPostHeader'))
{
	 function ApiPostHeader($apiType, $apiValue) {				
		
		regenerate_token:
		$ci = get_instance(); 
		if(!isset($_SESSION['AccessToken']) || $_SESSION['AccessToken']=='') {
			$params = array('Proj_ID'=>'103043');
			$arrAccessInfo = ApiAccessTokenHeader($ci->config->item('apiHostServer').$ci->config->item('GetAccessToken'), $params);
			//echo '<pre>';print_r($params);print_r($arrAccessInfo);echo 'yes'.$ci->config->item('apiHostServer').$ci->config->item('GetAccessToken');exit;
			if($arrAccessInfo['errcode']=='0'){
				if(!isset($_SESSION)) {
					session_start();
				}
				$_SESSION['AccessToken'] = trim($arrAccessInfo['tokenlist']['AccessTokenID']);											
			}			
		}
		$apiURL = $apiType;       		
		$apiHeader = array();
		$apiPostUrl = $ci->config->item('apiHostServer').$_SESSION['AccessToken'].$apiURL; 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $apiPostUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($apiValue));
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $apiHeader);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		//curl_setopt($ch,CURLOPT_TIMEOUT, 180);
		curl_setopt($ch,CURLOPT_TIMEOUT, 600);
		$output = curl_exec($ch);
		curl_close($ch);								
		$arrResult = json_decode($output, true);
		//echo '<pre>';echo 'hai'.$arrResult['Message'];exit;				
		//print_r(json_decode($output, true));exit;
		if (isset($arrResult['Message']) && $arrResult['Message']!='') 
		{
			unset($_SESSION['AccessToken']);
			goto regenerate_token;
		}else{
			return $arrResult;
		}		
	}
}

if ( ! function_exists('ApiGetHeader')){
	 function ApiGetHeader($apiType, $apiValue) {	

		regenerate_token:
		$ci = get_instance(); 
		if(!isset($_SESSION['AccessToken']) || $_SESSION['AccessToken']=='') {
			$params = array('Proj_ID'=>'103043');
			$arrAccessInfo = ApiAccessTokenHeader($ci->config->item('apiHostServer').$ci->config->item('GetAccessToken'), $params);
			//echo '<pre>';print_r($arrAccessInfo);echo 'yes'.$ci->config->item('GetAccessToken');exit;
			if($arrAccessInfo['errcode']=='0'){
				if(!isset($_SESSION)) {
					session_start();
				}
				$_SESSION['AccessToken'] = trim($arrAccessInfo['tokenlist']['AccessTokenID']);											
			}			
		}
	 
		$apiURL = $apiType;        
		@$apiURL = $apiURL.'?'.http_build_query($apiValue, '', '&');
				
		$apiHeader = array();
		$apiGetUrl = $ci->config->item('apiHostServer').$_SESSION['AccessToken'].$apiURL; 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $apiGetUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $apiHeader);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		//curl_setopt($ch,CURLOPT_TIMEOUT, 180);
		curl_setopt($ch,CURLOPT_TIMEOUT, 600);
		$output = curl_exec($ch);
		curl_close($ch);
		//echo '<pre>';print_r($output);
		//print_r(json_decode($output, true));
		$arrResult = json_decode($output, true);
		//echo '<pre>';echo 'hai'.$arrResult['Message'];exit;				
		//print_r(json_decode($output, true));exit;
		if (isset($arrResult['Message']) && $arrResult['Message']!='') 
		{
			unset($_SESSION['AccessToken']);
			goto regenerate_token;
		}else{
			return $arrResult;
		}		
    }
}


	function sendMail($to,$subject,$message){
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers		
		$headers .= 'From: Roaming Activation System <noreply@vectone.com>' . "\r\n";
		$headers .= 'To: ' . "\r\n";
		$headers .= 'Cc: ' . "\r\n";
		$headers .= 'Bcc: ' . "\r\n";

		// Mail it
		mail($to, $subject, $message, $headers);
		return true;
	}

// --------------------------------------------------------------------



/* End of file api_helper.php */
/* Location: ./system/helpers/api_helper.php */